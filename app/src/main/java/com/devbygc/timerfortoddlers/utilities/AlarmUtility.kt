package com.devbygc.timerfortoddlers.utilities

import android.app.AlarmManager
import android.app.Application
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.text.format.DateUtils
import android.util.Log
import com.devbygc.timerfortoddlers.AlarmBroadcastReceiver
import com.devbygc.timerfortoddlers.model.Alarm
import java.text.SimpleDateFormat
import java.util.*

private const val TAG = "GJCAlarmService"

class AlarmUtility private constructor(application: Application) {
    private val alarmMgr = application.getSystemService(Context.ALARM_SERVICE) as AlarmManager
    // Intent to start the Broadcast Receiver
    private val broadcastIntent = Intent(application, AlarmBroadcastReceiver::class.java)
    // The Pending Intent to pass in AlarmUtility
    private val pIntent = PendingIntent.getBroadcast(application, 0, broadcastIntent, 0)

    private var alarmCalendar = Calendar.getInstance()
    private var now = Calendar.getInstance()
    private var today = now.get(Calendar.DAY_OF_WEEK)

    private val dateTimeFormat = SimpleDateFormat("EEE, MMM d, yyyy @ h:mm a", Locale.US)
    private val timeFormat = SimpleDateFormat("h:mm a", Locale.US)
    private val daysOfWeekMap = mutableMapOf(
        '1' to "Su",
        '2' to "M",
        '3' to "T",
        '4' to "W",
        '5' to "Th",
        '6' to "F",
        '7' to "Sa"
    )
    private val dailySet = setOf('1', '2', '3', '4', '5', '6', '7')
    private val weekendSet = setOf('1', '7')
    private val weekdaySet = setOf('2', '3', '4', '5', '6')

    companion object {
        @Volatile private var instance: AlarmUtility? = null

        fun getInstance(application: Application) =
                instance ?: synchronized(this) {
                    instance ?: AlarmUtility(application).also { instance = it }
                }
    }

    fun setAlarm(alarm: Alarm) {
        var time: Long
        alarm.timeText = getHumanFriendlyAlarmTime(alarm)
        alarm.frequencyString = getHumanFriendlyAlarmFrequencyString(alarm)

        if (alarm.isActive) {
            time = validateTime(alarm)
            alarm.time = time
            registerAlarm(time)
            alarm.description = getHumanFriendlyAlarmDescription(time)
        } else {
            cancelAlarm()
        }
    }

    private fun validateTime(alarm: Alarm): Long {
        var frequencySet = mutableSetOf<Int>()
        alarm.frequency.forEach {
            frequencySet.add(it.toInt() - 48)
        }
        frequencySet = frequencySet.toSortedSet()

        now.timeInMillis = System.currentTimeMillis()
        today = now.get(Calendar.DAY_OF_WEEK)
        alarmCalendar.timeInMillis = System.currentTimeMillis()
        alarmCalendar.set(Calendar.HOUR_OF_DAY, alarm.hour)
        alarmCalendar.set(Calendar.MINUTE, alarm.minute)
        alarmCalendar.set(Calendar.SECOND, 0)
        alarmCalendar.set(Calendar.MILLISECOND, 0)

        // Match alarm frequency
        if (frequencySet.contains(today) && alarmCalendar.after(now)) {
            return alarmCalendar.timeInMillis
        }

        if (frequencySet.last() <= today) {
            alarmCalendar.add(Calendar.WEEK_OF_YEAR, 1)
            alarmCalendar.set(Calendar.DAY_OF_WEEK, frequencySet.first())
            return alarmCalendar.timeInMillis
        }
        val nextFrequencyDay: Int? = frequencySet.first { it > today }
        if (nextFrequencyDay != null) {
            alarmCalendar.set(Calendar.DAY_OF_WEEK, frequencySet.first { it > today})
        }

        return alarmCalendar.timeInMillis
    }

    private fun registerAlarm(time: Long) {
        when {
            Build.VERSION.SDK_INT >= 23 -> alarmMgr.setExactAndAllowWhileIdle(
                AlarmManager.RTC_WAKEUP,
                time,
                pIntent
            )
            Build.VERSION.SDK_INT >= 19 -> alarmMgr.setExact(
                AlarmManager.RTC_WAKEUP,
                time,
                pIntent
            )
            else -> alarmMgr.set(
                AlarmManager.RTC_WAKEUP,
                time,
                pIntent
            )
        }
    }

    fun cancelAlarm() {
        alarmMgr.cancel(pIntent)
    }

    fun getHumanFriendlyAlarmFrequencyString(alarm: Alarm): String {
        var humanFriendlyFrequencyString = ""
        val frequencyString = alarm.frequency

        if (frequencyString != "") {
            val frequencySet = frequencyString.toSortedSet()
            var previous = '1'
            var isDateRange = false

            if (frequencySet == dailySet) return "Daily"
            if (frequencySet == weekdaySet) return "Weekdays"
            if (frequencySet == weekendSet) return "Weekends"
            if (frequencySet.size == 1) return "${daysOfWeekMap[frequencySet.first()]}"

            frequencySet.forEach {

                if (it == frequencySet.first()) {
                    humanFriendlyFrequencyString = "${daysOfWeekMap[it]}"
                } else if (it.toInt() == previous.toInt() + 1) {
                    isDateRange = true

                    // If the current range number is the last of frequencySet, add it to the string
                    if (it == frequencySet.last()) {
                        humanFriendlyFrequencyString += "-${daysOfWeekMap[it]}"
                    }

                } else if (isDateRange) {
                    humanFriendlyFrequencyString += "-${daysOfWeekMap[previous]}, ${daysOfWeekMap[it]}"
                    isDateRange = false
                } else {
                    humanFriendlyFrequencyString += ", ${daysOfWeekMap[it]}"
                }

                previous = it

            }

        } else {
            humanFriendlyFrequencyString = "Set Frequency"
        }

        return humanFriendlyFrequencyString
    }

    fun getHumanFriendlyAlarmDescription(time: Long): String {
        var humanFriendlyAlarmString = "The alarm will display "
        humanFriendlyAlarmString += when {
            DateUtils.isToday(time) -> "today @ ${timeFormat.format(time)}"
            isTomorrow(time) -> "tomorrow @ ${timeFormat.format(time)}"
            else -> "on ${dateTimeFormat.format(time)}"
        }

        return humanFriendlyAlarmString
    }

    fun getHumanFriendlyAlarmTime(alarm: Alarm): String {
        alarmCalendar.timeInMillis = System.currentTimeMillis()
        alarmCalendar.set(Calendar.HOUR_OF_DAY, alarm.hour)
        alarmCalendar.set(Calendar.MINUTE, alarm.minute)

        return timeFormat.format(alarmCalendar.timeInMillis)
    }

    private fun isTomorrow(time: Long): Boolean {
        return DateUtils.isToday(time - DateUtils.DAY_IN_MILLIS)
    }
}