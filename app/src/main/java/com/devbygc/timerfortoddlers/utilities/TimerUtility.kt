package com.devbygc.timerfortoddlers.utilities

import android.app.AlarmManager
import android.app.Application
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import com.devbygc.timerfortoddlers.TimerBroadcastReceiver
import com.devbygc.timerfortoddlers.model.Timer
import java.text.SimpleDateFormat
import java.util.*

private const val TAG = "GJCTimerService"

class TimerUtility private constructor(application: Application) {
    private val alarmMgr = application.getSystemService(Context.ALARM_SERVICE) as AlarmManager
    // Intent to start the Broadcast Receiver
    private val broadcastIntent = Intent(application, TimerBroadcastReceiver::class.java)
    // The Pending Intent to pass in AlarmUtility
    private val pIntent = PendingIntent.getBroadcast(application, 1, broadcastIntent, 0)

    private var timerCalendar = Calendar.getInstance()
    private val timeFormat = SimpleDateFormat("h:mm a", Locale.US)

    companion object {
        @Volatile private var instance: TimerUtility? = null

        fun getInstance(application: Application) =
                instance ?: synchronized(this) {
                    instance ?: TimerUtility(application).also { instance = it }
                }
    }

    fun setTimer(timer: Timer) {
        timerCalendar.timeInMillis = System.currentTimeMillis()
        timerCalendar.add(Calendar.MINUTE, timer.minute)
        val time = timerCalendar.timeInMillis

        registerTimer(time)
        timer.description = getHumanFriendlyDescription(time)

    }

    fun cancelTimer() {
        alarmMgr.cancel(pIntent)
    }

    private fun registerTimer(time: Long) {
        when {
            Build.VERSION.SDK_INT >= 23 -> alarmMgr.setExactAndAllowWhileIdle(
                AlarmManager.RTC_WAKEUP,
                time,
                pIntent
            )
            Build.VERSION.SDK_INT >= 19 -> alarmMgr.setExact(
                AlarmManager.RTC_WAKEUP,
                time,
                pIntent
            )
            else -> alarmMgr.set(
                AlarmManager.RTC_WAKEUP,
                time,
                pIntent
            )
        }
    }

    private fun getHumanFriendlyDescription(time: Long): String {
        return "The timer will display @ ${timeFormat.format(time)}"
    }

}