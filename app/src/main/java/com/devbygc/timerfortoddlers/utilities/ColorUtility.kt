package com.devbygc.timerfortoddlers.utilities

import android.graphics.Color

private const val TAG = "GJCColorUtility"

class ColorUtility private constructor() {

    private val colorMap = mapOf(
        "RED" to "#B71C1C",
        "ORANGE" to "#FF8000",
        "YELLOW" to "#EEEE00",
        "GREEN" to "#00FF00",
        "BLUE" to "#00BFFF",
        "PURPLE" to "#ab47bc",
        "PINK" to "#FF80AB"
    )

    fun getColorInt(color: String): Int {
        return Color.parseColor(colorMap[color.toUpperCase()])
    }

    companion object {
        @Volatile private var instance: ColorUtility? = null

        fun getInstance() =
                instance ?: synchronized(this) {
                    instance ?: ColorUtility().also { instance = it }
                }
    }



}