package com.devbygc.timerfortoddlers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat
import com.devbygc.timerfortoddlers.view.AlarmActivity

class AlarmBroadcastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent?) {

        AlertWakeLock.acquireWakeLock(context)

        val newIntent = Intent(context, AlarmActivity::class.java)
        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        ContextCompat.startActivity(context, newIntent, null)
    }

}