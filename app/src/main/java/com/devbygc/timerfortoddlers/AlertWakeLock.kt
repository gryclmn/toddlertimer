package com.devbygc.timerfortoddlers

import android.content.Context
import android.os.PowerManager
import android.util.Log

private const val TAG = "GJCAlertWakeLock"

class AlertWakeLock {

    companion object {

        private var wakeLock: PowerManager.WakeLock? = null

        fun acquireWakeLock(context: Context) {
            Log.d(TAG,"Acquiring cpu wake lock")
            if (wakeLock != null) {
                return
            }

            val pm = context.getSystemService(Context.POWER_SERVICE) as PowerManager

            wakeLock = pm.newWakeLock(
                PowerManager.PARTIAL_WAKE_LOCK or
                        PowerManager.ACQUIRE_CAUSES_WAKEUP or
                        PowerManager.ON_AFTER_RELEASE, "ToddlerTimer:WakeLock"
            )
            wakeLock!!.acquire(30*60*1000L /* 30 minutes */)
        }

        fun releaseWakeLock() {
            if (wakeLock != null) {
                if (wakeLock!!.isHeld) {
                    wakeLock!!.release()
                    wakeLock = null
                } else {
                    // Do nothing
                }
            }
        }
    }
}