package com.devbygc.timerfortoddlers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat.startActivity
import com.devbygc.timerfortoddlers.view.TimerActivity

class TimerBroadcastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent?) {

        AlertWakeLock.acquireWakeLock(context)

        val newIntent = Intent(context, TimerActivity::class.java)
        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(context, newIntent, null)
    }

}