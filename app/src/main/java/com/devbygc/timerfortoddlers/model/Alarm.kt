package com.devbygc.timerfortoddlers.model

private const val TAG = "Alarm"

class Alarm(
    var hour: Int = 7,
    var minute: Int = 0,
    var time: Long = System.currentTimeMillis(),
    var timeText: String = "7:00 AM",
    var color: Int = -16711936,
    var colorString: String = "GREEN",
    var isActive: Boolean = false,
    var frequency: String = "1234567",
    var frequencyString: String = "Daily"
) {
    var description: String = ""

    override fun toString(): String {
        return "Alarm: hour=$hour, minute=$minute, timeText=$timeText, color=$color, isAcitve=$isActive, frequency=$frequency, frequencyString=$frequencyString, description=$description"
    }
}
