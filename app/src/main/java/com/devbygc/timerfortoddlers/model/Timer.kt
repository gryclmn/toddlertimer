package com.devbygc.timerfortoddlers.model

private const val TAG = "GJCAlarm"

class Timer (
    var minute: Int = 70,
    var timeText: String = "",
    var color: Int = -16711936,
    var colorString: String = "GREEN",
    var isActive: Boolean = false,
    var description: String = ""
) {
    override fun toString(): String {
        return "Timer: minute=$minute, timeText=$timeText, color=$color, isAcitve=$isActive, description=$description"
    }
}