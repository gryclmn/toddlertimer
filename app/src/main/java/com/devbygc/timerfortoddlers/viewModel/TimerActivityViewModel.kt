package com.devbygc.timerfortoddlers.viewModel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.devbygc.timerfortoddlers.Repository
import com.devbygc.timerfortoddlers.utilities.TimerUtility
import com.devbygc.timerfortoddlers.model.Timer

private const val TAG = "GJCTimerActivityViewModel"

class TimerActivityViewModel(application: Application) : AndroidViewModel(application) {
    private val repository = Repository.getInstance(application)
    private var timer: Timer
    private val timerUtility = TimerUtility.getInstance(application)

    init {
        timer = repository.getTimer()
    }

    fun getTimerColor(): Int {
        return timer.color
    }

    fun deactivateTimer() {
        timer.isActive = false
        timerUtility.cancelTimer()
        timer.description = ""
        repository.saveTimer(timer)
    }
}