package com.devbygc.timerfortoddlers.viewModel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.devbygc.timerfortoddlers.utilities.AlarmUtility
import com.devbygc.timerfortoddlers.Repository
import com.devbygc.timerfortoddlers.model.Alarm

private const val TAG = "GJCAlarmActivityVM"

class AlarmActivityViewModel(application: Application) : AndroidViewModel(application) {
    private val repository = Repository.getInstance(application)
    private var alarm: Alarm
    private val alarmUtility = AlarmUtility.getInstance(application)

    init {
        alarm = repository.getAlarm()
    }

    fun getAlarmColor(): Int {
        return alarm.color
    }

    fun rescheduleAlarm() {
        alarmUtility.cancelAlarm()
        alarmUtility.setAlarm(alarm)
        repository.saveAlarm(alarm)
    }
}