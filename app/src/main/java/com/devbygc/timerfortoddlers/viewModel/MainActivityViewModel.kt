package com.devbygc.timerfortoddlers.viewModel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.devbygc.timerfortoddlers.utilities.AlarmUtility
import com.devbygc.timerfortoddlers.Repository
import com.devbygc.timerfortoddlers.utilities.TimerUtility
import com.devbygc.timerfortoddlers.model.Alarm
import com.devbygc.timerfortoddlers.model.Timer

@Suppress("unused")
private const val TAG = "GJCMainActivityVM"

class MainActivityViewModel(application: Application) : AndroidViewModel(application) {
    private val repository = Repository.getInstance(application)
    var alarmLiveData = MutableLiveData<Alarm>()
    private var alarm: Alarm
    private val alarmUtility = AlarmUtility.getInstance(application)
    var timerLiveData = MutableLiveData<Timer>()
    private var timer: Timer
    private val timerUtility = TimerUtility.getInstance(application)

    init {
        alarm = repository.getAlarm()
        setAlarmDescription()
        alarmLiveData.value = alarm
        timer = repository.getTimer()
        timerLiveData.value = timer
    }

    fun refresh() {
        alarm = repository.getAlarm()
        setAlarmDescription()
        alarmLiveData.value = alarm
        timer = repository.getTimer()
        timerLiveData.value = timer
    }

    private fun saveAlarm(newAlarm: Alarm) {
        repository.saveAlarm(newAlarm)
        alarmLiveData.value = newAlarm
    }

    fun getAlarmFrequency(): String {
        return alarm.frequency
    }

    fun setAlarmFrequency(newFrequencyString: String) {
        if (alarm.isActive) alarm.isActive = false
        alarm.frequency = newFrequencyString

        if (newFrequencyString == "") {
            alarmUtility.cancelAlarm()
        } else {
            alarm.frequencyString = alarmUtility.getHumanFriendlyAlarmFrequencyString(alarm)
        }

        saveAlarm(alarm)
    }

    fun getAlarmHour(): Int {
        return alarm.hour
    }

    fun getAlarmMinute(): Int {
        return alarm.minute
    }

    fun setAlarmTime(newHour: Int, newMinute: Int) {
        if (alarm.isActive) alarm.isActive = false
        alarm.hour = newHour
        alarm.minute = newMinute
        alarm.timeText = alarmUtility.getHumanFriendlyAlarmTime(alarm)
        saveAlarm(alarm)
    }

    fun setAlarmIsActive(newIsActive: Boolean) {
        alarm.isActive = newIsActive

        if (newIsActive) {
            alarmUtility.setAlarm(alarm)
        } else {
            alarmUtility.cancelAlarm()
            alarm.description = ""
        }
        saveAlarm(alarm)
    }

    fun getAlarmColorString(): String {
        return alarm.colorString
    }

    fun setAlarmColor(newColor: Int, newColorString: String) {
        alarm.color = newColor
        alarm.colorString = newColorString
        saveAlarm(alarm)
    }

    fun setAlarmDescription() {
        if (alarm.isActive) {
            alarm.description = alarmUtility.getHumanFriendlyAlarmDescription(alarm.time)
        }
    }

    private fun saveTimer(newTimer: Timer) {
        repository.saveTimer(newTimer)
        timerLiveData.value = newTimer
    }

    fun getTimerMinute(): Int {
        return timer.minute
    }

    fun setTimerMinute(newMinute: Int) {
        timer.minute = newMinute
        timer.timeText = "$newMinute min"
        saveTimer(timer)
    }

    fun getTimerColorString(): String {
        return timer.colorString
    }

    fun setTimerColor(newColor: Int, newColorString: String) {
        timer.color = newColor
        timer.colorString = newColorString
        saveTimer(timer)
    }

    fun setTimerIsActive() {
        timer.isActive = !timer.isActive
        if (timer.isActive) {
            timerUtility.setTimer(timer)
        } else {
            timerUtility.cancelTimer()
            timer.description = ""
        }
        saveTimer(timer)
    }

}