package com.devbygc.timerfortoddlers.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.devbygc.timerfortoddlers.R
import com.devbygc.timerfortoddlers.model.Alarm
import com.devbygc.timerfortoddlers.model.Timer
import com.devbygc.timerfortoddlers.viewModel.MainActivityViewModel
import kotlinx.android.synthetic.main.activity_main.*

@Suppress("unused")
private const val TAG = "GJCMainActivity"

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val model = ViewModelProviders.of(this).get(MainActivityViewModel::class.java)

        val alarmObserver = Observer<Alarm> { newAlarm ->
            if (newAlarm != null) {
                alarmFrequencyBtn.text = newAlarm.frequencyString
                alarmTimeBtn.text = newAlarm.timeText
                alarmColorBtn.setTextColor(newAlarm.color)
                alarmIsActiveSwitch.isChecked = newAlarm.isActive
                alarmDescriptionTextView.text = newAlarm.description
            }

        }
        model.alarmLiveData.observe(this, alarmObserver)

        val timerObserver = Observer<Timer> { newTimer ->
            if (newTimer != null) {
                timerTimeBtn.text = newTimer.timeText
                timerColorBtn.setTextColor(newTimer.color)
                timerDescriptionTextView.text = newTimer.description
                if (newTimer.isActive) {
                    timerStartBtn.text = "cancel"
                } else {
                    timerStartBtn.text = "start"
                }

            }

        }
        model.timerLiveData.observe(this, timerObserver)

        val frequencyPickerFragment = FrequencyPickerFragment.newInstance()
        val timePickerFragment = TimePickerFragment.newInstance()
        val alarmColorPickerFragment = ColorPickerFragment.newInstance("alarm")

        alarmFrequencyBtn.setOnClickListener {
            if (!frequencyPickerFragment.isVisible) {
                frequencyPickerFragment.show(supportFragmentManager, "frequencyPicker")
            }
        }

        alarmTimeBtn.setOnClickListener {
            if (!timePickerFragment.isVisible) {
                timePickerFragment.show(supportFragmentManager, "timePicker")
            }
        }

        alarmColorBtn.setOnClickListener {
            if (!alarmColorPickerFragment.isVisible) {
                alarmColorPickerFragment.show(supportFragmentManager, "alarmColorPicker")
            }
        }

        alarmIsActiveSwitch.setOnCheckedChangeListener { compoundButton, isChecked ->
            model.setAlarmIsActive(isChecked)
        }

        val minutePickerFragment = MinutePickerFragment.newInstance()
        val timerColorPickerFragment = ColorPickerFragment.newInstance("timer")

        timerTimeBtn.setOnClickListener {
            if (!minutePickerFragment.isVisible) {
                minutePickerFragment.show(supportFragmentManager, "minutePicker")
            }
        }

        timerColorBtn.setOnClickListener {
            if (!timerColorPickerFragment.isVisible) {
                timerColorPickerFragment.show(supportFragmentManager, "timerColorPicker")
            }
        }

        timerStartBtn.setOnClickListener {
            model.setTimerIsActive()
        }

    }

    override fun onResume() {
        super.onResume()
        val model = ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
        model.refresh()
    }
}
