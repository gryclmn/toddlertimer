package com.devbygc.timerfortoddlers.view

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import com.devbygc.timerfortoddlers.R
import com.devbygc.timerfortoddlers.viewModel.MainActivityViewModel
import kotlinx.android.synthetic.main.fragment_frequency_picker.*

@Suppress("unused")
private const val TAG = "GJCFreqPickerFragment"
/**
 * A simple Fragment subclass.
 * Use the FrequencyPickerFragment.newInstance factory method to
 * create an instance of this fragment.
 *
 */
class FrequencyPickerFragment : DialogFragment() {
    private lateinit var model: MainActivityViewModel
    private val checkboxMap = mutableMapOf<String, CheckBox>()
    private lateinit var frequency: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        model = activity?.run {
            ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        frequency = model.getAlarmFrequency()

        arguments?.let {
            // Access Bundle values here
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_frequency_picker, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        checkboxMap["1"] = checkBox
        checkboxMap["2"] = checkBox2
        checkboxMap["3"] = checkBox3
        checkboxMap["4"] = checkBox4
        checkboxMap["5"] = checkBox5
        checkboxMap["6"] = checkBox6
        checkboxMap["7"] = checkBox7

        // Check boxes for days if contained in frequency
        frequency.forEach {
            when (it) {
                '1' -> checkBox.isChecked = true
                '2' -> checkBox2.isChecked = true
                '3' -> checkBox3.isChecked = true
                '4' -> checkBox4.isChecked = true
                '5' -> checkBox5.isChecked = true
                '6' -> checkBox6.isChecked = true
                '7' -> checkBox7.isChecked = true
            }
        }

        submitFreqBtn.setOnClickListener {
            this.dismiss()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        var frequencyString = ""

        checkboxMap.forEach {
            if (it.value.isChecked) frequencyString += it.key
        }

        model.setAlarmFrequency(frequencyString)

    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment FrequencyPickerFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
            FrequencyPickerFragment().apply {
                arguments = Bundle().apply {
                    // Assign bundle values here
                }
            }
    }
}
