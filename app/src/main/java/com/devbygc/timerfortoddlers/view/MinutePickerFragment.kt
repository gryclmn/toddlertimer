package com.devbygc.timerfortoddlers.view

import android.app.Dialog
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.devbygc.timerfortoddlers.R
import com.devbygc.timerfortoddlers.viewModel.MainActivityViewModel
import kotlinx.android.synthetic.main.fragment_minute_picker.*
import java.lang.Integer.parseInt

@Suppress("unused")
private const val TAG = "GJCMinutePickerFragment"
/**
 * A simple Fragment subclass.
 * Use the MinutePickerFragment.newInstance factory method to
 * create an instance of this fragment.
 *
 */
class MinutePickerFragment : DialogFragment() {
    private var minute = 0
    private lateinit var model: MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        model = activity?.run {
            ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        arguments?.let {
            // Access Bundle values here
        }

    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.fragment_minute_picker, null)

        val dialog = Dialog(context, R.style.ThemeOverlay_AppCompat_Dialog)
        dialog.setContentView(view)
        return dialog
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_minute_picker, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        minute = model.getTimerMinute()
        minuteTextView.text = minute.toString()

        numPadBtn_1.setOnClickListener { updateMinuteEditText(1)}
        numPadBtn_2.setOnClickListener { updateMinuteEditText(2)}
        numPadBtn_3.setOnClickListener { updateMinuteEditText(3)}
        numPadBtn_4.setOnClickListener { updateMinuteEditText(4)}
        numPadBtn_5.setOnClickListener { updateMinuteEditText(5)}
        numPadBtn_6.setOnClickListener { updateMinuteEditText(6)}
        numPadBtn_7.setOnClickListener { updateMinuteEditText(7)}
        numPadBtn_8.setOnClickListener { updateMinuteEditText(8)}
        numPadBtn_9.setOnClickListener { updateMinuteEditText(9)}
        numPadBtn_0.setOnClickListener { updateMinuteEditText(0)}
        backspaceBtn.setOnClickListener {
            val text = minuteTextView.text
            minuteTextView.text = text.dropLast(1)
        }

        backspaceBtn.setOnLongClickListener {
            minuteTextView.text = "0"
            true
        }

        submitColorBtn.setOnClickListener { dismiss() }

    }

    private fun updateMinuteEditText(num: Int) {
        val text = minuteTextView.text

        if (text == "0") {
            minuteTextView.text = num.toString()
        } else if (parseInt("$text$num")  > 600) {
            minuteTextView.text = "600"
        } else {
            minuteTextView.text = "$text$num"
        }

    }

    override fun dismiss() {
        super.dismiss()

        val text = minuteTextView.text.toString()

        if (text != "") {
            val newMinute = parseInt(text)
            if (newMinute > 0) model.setTimerMinute(newMinute) else model.setTimerMinute(1)
        } else {
            model.setTimerMinute(1)
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment MinutePickerFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
            MinutePickerFragment().apply {
                arguments = Bundle().apply {
                    // Assign bundle values here
                }
            }
    }
}
