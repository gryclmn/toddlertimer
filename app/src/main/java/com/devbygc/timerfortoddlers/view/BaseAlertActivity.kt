package com.devbygc.timerfortoddlers.view

import android.app.KeyguardManager
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.WindowManager
import com.devbygc.timerfortoddlers.AlertWakeLock
import java.util.*

private const val TAG = "GJCBaseAlertActivity"

open class BaseAlertActivity : AppCompatActivity() {

    val timer = Timer()

    @Suppress("DEPRECATION")
    fun turnScreenOn() {

        val k = this.getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
        val layout = window.attributes

        when {
            Build.VERSION.SDK_INT >= 27 -> {
                setShowWhenLocked(true)
                setTurnScreenOn(true)
                k.requestDismissKeyguard(this, null)
            }
            Build.VERSION.SDK_INT == 26 -> {
                window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED)
                k.requestDismissKeyguard(this, null)
                window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON)
                window.addFlags(WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON)
            }
            else -> {
                window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED)
                window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD)
                window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON)
                window.addFlags(WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON)
            }
        }

        layout.screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        timer.schedule(object : TimerTask() {
            override fun run() {
                Log.d(TAG, "@run() for timer")
                finish()
            }
        }, 30*60*1000L /* 30 minutes */)

    }

    override fun onDestroy() {
        super.onDestroy()
        timer.cancel()
        AlertWakeLock.releaseWakeLock()
    }
}