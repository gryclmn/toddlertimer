package com.devbygc.timerfortoddlers.view

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import com.devbygc.timerfortoddlers.viewModel.TimerActivityViewModel
import kotlinx.android.synthetic.main.activity_timer.*

private const val TAG = "GJCTimerActivity"

class TimerActivity : BaseAlertActivity() {

    lateinit var model: TimerActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.devbygc.timerfortoddlers.R.layout.activity_timer)

        model = ViewModelProviders.of(this).get(TimerActivityViewModel::class.java)
        model.deactivateTimer()
        val backgroundColor = model.getTimerColor()

        timerActivity.setBackgroundColor(backgroundColor)

        // turnScreenOn() is inherited from BaseAlertActivity
        turnScreenOn()

    }

}
