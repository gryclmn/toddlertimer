package com.devbygc.timerfortoddlers.view

import android.app.Dialog
import android.app.TimePickerDialog
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.widget.TimePicker
import com.devbygc.timerfortoddlers.viewModel.MainActivityViewModel

@Suppress("unused")
private const val TAG = "GJCTimePickerFragment"

/**
 * A simple Fragment subclass.
 * Use the TimePickerFragment.newInstance factory method to
 * create an instance of this fragment.
 *
 */
class TimePickerFragment : DialogFragment(), TimePickerDialog.OnTimeSetListener {
    private lateinit var model: MainActivityViewModel

    override fun onTimeSet(view: TimePicker, hour: Int, minute: Int) {
        model.setAlarmTime(hour, minute)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        model = activity?.run {
            ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        arguments?.let {
            // Access Bundle values here
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val hour = model.getAlarmHour()
        val minute = model.getAlarmMinute()

        // Create a new instance of TimePickerDialog and return it
        return TimePickerDialog(activity, this, hour, minute, false)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment TimePickerFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
            TimePickerFragment().apply {
                arguments = Bundle().apply {
                    // Assign bundle values here
                }
            }
    }
}
