package com.devbygc.timerfortoddlers.view

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import com.devbygc.timerfortoddlers.viewModel.AlarmActivityViewModel
import kotlinx.android.synthetic.main.activity_alarm.*

class AlarmActivity : BaseAlertActivity() {

    lateinit var model: AlarmActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.devbygc.timerfortoddlers.R.layout.activity_alarm)

        model = ViewModelProviders.of(this).get(AlarmActivityViewModel::class.java)
        model.rescheduleAlarm()
        val backgroundColor = model.getAlarmColor()

        alarmActivity.setBackgroundColor(backgroundColor)

        // turnScreenOn() is inherited from BaseAlertActivity
        turnScreenOn()

    }

}
