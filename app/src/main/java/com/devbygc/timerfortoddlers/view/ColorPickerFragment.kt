package com.devbygc.timerfortoddlers.view

import android.app.Dialog
import android.arch.lifecycle.ViewModelProviders
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import com.devbygc.timerfortoddlers.utilities.ColorUtility
import com.devbygc.timerfortoddlers.viewModel.MainActivityViewModel
import kotlinx.android.synthetic.main.fragment_color_picker.*


private const val TYPE = "type"
@Suppress("unused")
private const val TAG = "GJCColorPickerFragment"
/**
 * A simple Fragment subclass.
 * Use the ColorPickerFragment.newInstance factory method to
 * create an instance of this fragment.
 *
 */
class ColorPickerFragment : DialogFragment() {
    private lateinit var model: MainActivityViewModel
    private var type: String? = null
    lateinit var activeColorBtn: ImageButton
    private val colorUtility = ColorUtility.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        model = activity?.run {
            ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        arguments?.let {
            type = it.getString(TYPE)
        }

    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(com.devbygc.timerfortoddlers.R.layout.fragment_color_picker, null)

        val dialog = Dialog(context, com.devbygc.timerfortoddlers.R.style.ThemeOverlay_AppCompat_Dialog)
        dialog.setContentView(view)
        val width = (resources.displayMetrics.widthPixels * 0.75).toInt()
        val height = (resources.displayMetrics.heightPixels * 0.75).toInt()
        dialog.window?.setLayout(width, height)
        return dialog
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(com.devbygc.timerfortoddlers.R.layout.fragment_color_picker, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val activeColor = if (type == "alarm") model.getAlarmColorString() else model.getTimerColorString()
        when (activeColor) {
            "RED" -> activeColorBtn = colorPickerLayout.findViewById(redBtn.id)
            "ORANGE" -> activeColorBtn = colorPickerLayout.findViewById(orangeBtn.id)
            "BLUE" -> activeColorBtn = colorPickerLayout.findViewById(greenBtn.id)
            "GREEN" -> activeColorBtn = colorPickerLayout.findViewById(blueBtn.id)
            "YELLOW" -> activeColorBtn = colorPickerLayout.findViewById(yellowBtn.id)
            "PURPLE" -> activeColorBtn = colorPickerLayout.findViewById(purpleBtn.id)
            "PINK" -> activeColorBtn = colorPickerLayout.findViewById(pinkBtn.id)
        }
        activeColorBtn.setBackgroundColor(colorUtility.getColorInt(activeColor))

        setUpColorButton(redBtn,"red")
        setUpColorButton(orangeBtn,"orange")
        setUpColorButton(yellowBtn,"yellow")
        setUpColorButton(blueBtn,"green")
        setUpColorButton(greenBtn,"blue")
        setUpColorButton(purpleBtn,"purple")
        setUpColorButton(pinkBtn,"pink")

        submitColorBtn.setOnClickListener { dismiss() }

    }

    private fun setUpColorButton(btn: ImageButton, color: String) {
        btn.drawable.setColorFilter(colorUtility.getColorInt(color), PorterDuff.Mode.MULTIPLY)
        btn.setOnClickListener { handleClick(btn, color) }
    }

    private fun handleClick(btn: ImageButton, color: String = "") {

        if (btn == activeColorBtn) {
            // do nothing
        } else {
            val newColorInt = colorUtility.getColorInt(color)
            if (type == "alarm") {
                model.setAlarmColor(newColorInt, color.toUpperCase())
            } else {
                model.setTimerColor(newColorInt, color.toUpperCase())
            }
            btn.setBackgroundColor(colorUtility.getColorInt(color))
            activeColorBtn.setBackgroundColor(Color.TRANSPARENT)
            activeColorBtn = btn
        }

    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param type identifies if ColorPickerFragment is launched for timer or alarm.
         * @return A new instance of fragment ColorPickerFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(type: String) =
            ColorPickerFragment().apply {
                arguments = Bundle().apply {
                    putString(TYPE, type)
                }
            }
    }
}
