package com.devbygc.timerfortoddlers

import android.app.Application
import android.preference.PreferenceManager
import android.util.Log
import com.devbygc.timerfortoddlers.model.Alarm
import com.devbygc.timerfortoddlers.model.Timer

private const val TAG = "GJCRepository"

class Repository private constructor(application: Application) {
    private val sharedPref = PreferenceManager.getDefaultSharedPreferences(application)
    private var alarm = Alarm()
    private var timer = Timer()

    fun getAlarm(): Alarm {
        alarm = Alarm(
            sharedPref.getInt("alarmHour", 7),
            sharedPref.getInt("alarmMinute", 0),
            sharedPref.getLong("alarmTime", System.currentTimeMillis()),
            sharedPref.getString("alarmTimeText", "7:00 AM"),
            sharedPref.getInt("alarmColor", -16711936),
            sharedPref.getString("alarmColorString", "GREEN"),
            sharedPref.getBoolean("alarmIsActive", false),
            sharedPref.getString("alarmFrequency", "1234567"),
            sharedPref.getString("alarmFrequencyString", "Daily")
        )

    return alarm
    }

    fun saveAlarm(newAlarm: Alarm) {
        with (sharedPref.edit()) {
            putInt("alarmHour", newAlarm.hour)
            putInt("alarmMinute", newAlarm.minute)
            putLong("alarmTime", newAlarm.time)
            putString("alarmTimeText", newAlarm.timeText)
            putInt("alarmColor", newAlarm.color)
            putString("alarmColorString", newAlarm.colorString)
            putBoolean("alarmIsActive", newAlarm.isActive)
            putString("alarmFrequency", newAlarm.frequency)
            putString("alarmFrequencyString", newAlarm.frequencyString)

            apply()
        }

    }

    fun getTimer(): Timer {
        timer = Timer(
            sharedPref.getInt("timerMinute", 90),
            sharedPref.getString("timerTimeText", "90 min"),
            sharedPref.getInt("timerColor", -16711936),
            sharedPref.getString("timerColorString", "GREEN"),
            sharedPref.getBoolean("timerIsActive", false),
            sharedPref.getString("timerDescription", "")
        )

        return timer
    }

    fun saveTimer(newTimer: Timer) {
        with (sharedPref.edit()) {
            putInt("timerMinute", newTimer.minute)
            putString("timerTimeText", newTimer.timeText)
            putInt("timerColor", newTimer.color)
            putString("timerColorString", newTimer.colorString)
            putBoolean("timerIsActive", newTimer.isActive)
            putString("timerDescription", newTimer.description)

            apply()
        }

    }

    companion object {
        @Volatile private var instance: Repository? = null

        fun getInstance(application: Application) =
                instance ?: synchronized(this) {
                    instance ?: Repository(application).also { instance = it }
                }
    }

}